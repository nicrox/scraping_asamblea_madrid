const colors = require('colors');
const fs = require('fs');
const download = require('download');
const mkdirp = require('mkdirp');
const path = require('path');
const config = require('./config');
const outputFiles = require('./outputFiles');

let counterFiles = 0;
let counterChunks = 1;
//let totalCounter = 1;

/*3 - Descargamos el archivo */
const fileDownload = async (url, legislatura, name) => {

  try {

    console.log('Accediendo:'.yellow, url);
    await download(url, 'input/' + legislatura);
    console.log(' Descargado:'.green, name);

  } catch (e) {

    console.log('Archivo no encontrado:'.red, url);
  }

    /*4 - Conversion */
    await outputFiles(name, legislatura);
    counterChunks++;
    counterFiles++;

    /* Finalizacion */
    if (counterFiles === config.documentos) {
      console.log('Descarga finalizada!!'.green);
      process.exit();
    };
    
    /* bucle */
    if (counterChunks === config.chunks) {
      executeScraping(false);
     }

}

/*2 - Ejecutamos el scraping */
const executeScraping = async (flag) => {

  if (counterChunks === config.chunks || flag) {

    counterChunks = 0  //reset el chunck;

    console.log('Iniciamos Iteracion...');

    let fileArray = [];

    /* Iteramos */
    for (c = 1; c <= config.chunks; c++) {
      fileArray.push(`${config.legislatura}-DS-${counterFiles + c}.pdf`);
    }

    fileArray.forEach((value, index) => {
      fileDownload(config.urlBase + value, config.legislatura, value);
    });

    /* Chequeo */
    //setInterval(() => executeScraping(false), config.speed);
    //setTimeout(() => {
    //  executeScraping(false)
    //}, 5000);

  }

}

/*1 - Creamos la carpeta*/
mkdirp(`./output/${config.documentType}/${config.legislatura}`).then((made) => {

  console.log(`Creamos directorio ${made}`);

  executeScraping(true);

});





