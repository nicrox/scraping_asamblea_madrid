const configuration = {

  
  // Acceso y Filtro
  webSite: 'https://www.asambleamadrid.es/servicios/publicaciones/ddss',
  urlBase: 'https://www.asambleamadrid.es/static/doc/publicaciones/',
  splitText: '(Se abre la sesión ',
  documentTypeText: 'Sesión Plenaria',
  documentType:'Plenaria',

  // Datos
  legislatura: 'IV',
  documentos: 796,

  // Performance
  chunks: 5,
  speed: 3000,
    
};

module.exports = configuration;

