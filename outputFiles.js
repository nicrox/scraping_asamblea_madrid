const colors = require('colors');
const config = require('./config');
const fs = require('fs');
const pdf = require('pdf-parse');
const path = require('path');

const converterFiles = async (name, legislatura) => {

  const fileInput = (file) => path.join(__dirname, 'input/' + legislatura, file)
  const fileOutput = (file) => path.join(__dirname, 'output/' + (config.documentType + '/' + legislatura), file)

  let dataBuffer = fs.readFileSync(fileInput(name));
  let textFile;
  const splitText = config.splitText;
  let documentType, infoBody , message;

  pdf(dataBuffer).then(function (data) {

    //console.log('**data.numpages: ', data.numpages);
    //console.log('**data.numrender:', data.numrender);
    //console.log('**data.info:', data.info);
    //console.log('**data.metadata:', data.metadata);
    //console.log('**data.version:', data.version);

    /* Comprobamos que el mensaje tiene cuerpo si no leemos el contenido*/
    if ((data.info).Subject !== undefined) {

      infoBody = (data.info).Subject;
      documentType = config.documentType;
      message = (data.info).Subject
      
    } else {

      //console.log('Sin cuerpo del mensaje'.red);
      infoBody = data.text;
      documentType = config.documentTypeText;
      message = (data.info).Title;

    }

    /* comprobamos que es tipo de documento que necesitamos */
    ///console.log('*Textos:',documentType);
    ///console.log('***Comprobamos,', (infoBody.indexOf(documentType) !== -1));
    if (infoBody.indexOf(documentType) !== -1) {

      /* Eliminamos contenido innecesario */
      const finalContent = (data.text).indexOf(splitText);
      textFile = (data.text).slice(finalContent, (data.text).length);

      /* Guardamos el archivo */
      const fileName = '[' + name + ']' + '[' + message + '].txt';
      fs.writeFile(fileOutput(fileName), textFile, function (err) {
        if (err) throw err;
        console.log('Covertido:'.magenta, fileName);
      });

    }
  });

};

module.exports = converterFiles;