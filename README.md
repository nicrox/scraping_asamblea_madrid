# scraping_asamblea_madrid

Descarga y conversión a texto de los plenos de las diferentes legislaturas.

Legislatura | documentos | Plenaria
-|-|-
XI|135|15
X|885|115
IX|779|112
VIII|857|114
VII|900|96
VI|39|5
V|876|107
IV|796|118
III||
II||
I||

##TO-DO
-Hay que revisar si desacargamos siempre los últimos elementos.
-Hay que comprobar que el archivo existe